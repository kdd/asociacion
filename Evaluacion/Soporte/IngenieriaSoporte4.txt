=== Run information ===

Scheme:       weka.associations.FPGrowth -P 2 -I -1 -N 100 -T 0 -C 0.9 -D 0.05 -U 586.0 -M 4.0
Relation:     cancelaciones_ingenieria
Instances:    586
Attributes:   178
[list of attributes omitted]
=== Associator model (full training set) ===

FPGrowth found 6 rules (displaying top 6)

1. [116053M   -LABORATORIO QUIMICA I-Q-=1]: 6 ==> [116052M   -QUIMICA I-Q-=1]: 6   <conf:(1)> lift:(48.83) lev:(0.01) conv:(5.88) 
2. [626044M   -ACTIVIDAD FISICA EN PREHOSPITALARIA II=1]: 7 ==> [607021M   -FRANJA SOCIAL EN SALUD I=1]: 7   <conf:(1)> lift:(53.27) lev:(0.01) conv:(6.87) 
3. [111050M   -CALCULO I=1, 710196M   -ELECTRICIDAD PARA ELECTRONICA=1]: 5 ==> [730107M   -INTRODUCCION A LA GESTION AMBIENTAL=1]: 5   <conf:(1)> lift:(9.93) lev:(0.01) conv:(4.5) 
4. [204161M   -COMPRENSION Y PRODUCCION DE TEXTOS ACADEMICOS=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 4 ==> [116052M   -QUIMICA I-Q-=1]: 4   <conf:(1)> lift:(48.83) lev:(0.01) conv:(3.92) 
5. [116052M   -QUIMICA I-Q-=1, 111061M   -MATEMATICA FUNDAMENTAL=1]: 4 ==> [116053M   -LABORATORIO QUIMICA I-Q-=1]: 4   <conf:(1)> lift:(97.67) lev:(0.01) conv:(3.96) 
6. [111061M   -MATEMATICA FUNDAMENTAL=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 4 ==> [116052M   -QUIMICA I-Q-=1]: 4   <conf:(1)> lift:(48.83) lev:(0.01) conv:(3.92) 

