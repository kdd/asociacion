package src;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class createDataAsociation {

    int cantidad = 0;
    String estudiante;
    int asignaturas[];
    FachadaBD fachada;
    BufferedWriter file;

    public createDataAsociation() {
        estudiante = "";
        cantidad = 0;
        fachada = new FachadaBD();

        try {
            file = new BufferedWriter(new PrintWriter(new FileWriter("DatosSaludCancelaciones.arff")));
            file.write("@relation 'cancelaciones_Salud  ' \n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void poblarDatosTablaAsignatura() {

        /*
         // Ciencias
        String sqlConsulta ="select codigo as cod_asig,nombre from"
        + " (select asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '31%')as a )as b) as c natural join "
        + "asignatura group by cod_asig,nombre order by cod_asig desc ;";
        */
        

        /*
        //Pedagogia 

         String sqlConsulta = "select codigo as cod_asig,nombre from "
        + "(select asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '34%')as a )as b) as c natural join "
        + "asignatura group by cod_asig,nombre order by cod_asig desc ;";
         */

        /*
        //Humanidades
        String sqlConsulta = "select codigo as cod_asig,nombre from "
        + "(select asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '32%')as a )as b) as c natural join "
        + "asignatura group by cod_asig,nombre order by cod_asig desc ;";
         */




        /*
        //Ingenieria
        String sqlConsulta = "select codigo as cod_asig, nombre from "
        + "(select asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from"
        + " programa where codigo like '2%' or codigo like '37')as a )as b) as c natural join"
        + " asignatura group by cod_asig,nombre order by cod_asig desc ;";
         */

        /*
        //Artes
        
        String sqlConsulta = "select codigo as cod_asig,nombre from "
        + "(select asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '35%')as a )as b) as c natural join "
        + "asignatura group by cod_asig,nombre order by cod_asig desc ;";
         */

        /*
        // Ciencias Sociales y Econocimicas
            String sqlConsulta = "select codigo as cod_asig,nombre from "
            + "(select asignatura_key  from "
            + "(select *  from cancelacion natural join "
            + "(select programa_key from programa where codigo like '33%')as a )as b) as c natural join "
            + "asignatura group by cod_asig,nombre order by cod_asig desc ;";
            * 
        */
         

        // Salud
        
        String sqlConsulta = "select codigo as cod_asig,nombre from "
        + "(select asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '36%')as a )as b) as c natural join "
        + "asignatura group by cod_asig,nombre order by cod_asig desc ;";
        
         
        /*
        // Ciencias de la administracion
        String sqlConsulta = "select codigo as cod_asig, nombre from "
        + "(select asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '38%')as a )as b) as c natural join "
        + "asignatura group by cod_asig,nombre  order by cod_asig desc ;";
        */


        /*"SELECT cod_asignatura FROM asignatura_temp";*/
        String codigo;
        String nombre;
        String sqlInsertar;

        try {
            Connection conn = fachada.conectar();
            Statement sentencia = conn.createStatement();
            Statement sentenciaUpdate = conn.createStatement();
            ResultSet tabla = sentencia.executeQuery(sqlConsulta);

            while (tabla.next()) {
                cantidad++;
                codigo = tabla.getString(1);
                nombre = tabla.getString(2);
                sqlInsertar = "INSERT INTO asignatura_temp_as VALUES (DEFAULT,'" + codigo + "')";
                sentenciaUpdate.executeUpdate(sqlInsertar);
                file.write("@attribute '" + codigo+"-"+nombre+"' {0,1} \n");
            }
            file.write("@data \n");
            fachada.cerrarConexion(conn);
        } catch (SQLException e) {
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    void crearDatos(int cantidad) {
        if (cantidad != -1) {
            this.cantidad = cantidad;
        }

        asignaturas = new int[this.cantidad];

        String sqlConsultaMatricula = "";

        /*
        //Ciencias
        sqlConsultaMatricula = "select cod_estudiante,id from"
        + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
        + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
        + "(select estudiante_key, asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '31%')as a )as b) as c natural join"
        + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
        + " asignatura_temp_as order by cod_estudiante ;";
         */

        /*
        //Humanidades
        sqlConsultaMatricula = "select cod_estudiante,id from"
        + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
        + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
        + "(select estudiante_key, asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '32%')as a )as b) as c natural join"
        + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
        + " asignatura_temp_as order by cod_estudiante ;";
         */
        
        /*
        //Ingenieria
        sqlConsultaMatricula = "select cod_estudiante,id from"
        + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
        + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
        + "(select estudiante_key, asignatura_key  from "
        + "(select *  from cancelacion natural join "
        + "(select programa_key from programa where codigo like '2%' or codigo like '37')as a )as b) as c natural join"
        + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
        + " asignatura_temp_as order by cod_estudiante ;";
          */  

        /*
        //Pedagogia
        
        sqlConsultaMatricula = "select cod_estudiante,id from"
                + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
                + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
                + "(select estudiante_key, asignatura_key  from "
                + "(select *  from cancelacion natural join "
                + "(select programa_key from programa where codigo like '34%')as a )as b) as c natural join"
                + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
                + " asignatura_temp_as order by cod_estudiante ;";
           */     
        



        /*
        //Artes
        
        sqlConsultaMatricula = "select cod_estudiante,id from"
                + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
                + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
                + "(select estudiante_key, asignatura_key  from "
                + "(select *  from cancelacion natural join "
                + "(select programa_key from programa where codigo like '35%')as a )as b) as c natural join"
                + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
                + " asignatura_temp_as order by cod_estudiante ;";
         */
        /*
        //Ciencias Sociales y Economicas
            
        sqlConsultaMatricula = "select cod_estudiante,id from"
                + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
                + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
                + "(select estudiante_key, asignatura_key  from "
                + "(select *  from cancelacion natural join "
                + "(select programa_key from programa where codigo like '33%')as a )as b) as c natural join"
                + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
                + " asignatura_temp_as order by cod_estudiante ;";
        */
         
        
        //Salud
        
        sqlConsultaMatricula = "select cod_estudiante,id from"
                + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
                + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
                + "(select estudiante_key, asignatura_key  from "
                + "(select *  from cancelacion natural join "
                + "(select programa_key from programa where codigo like '36%')as a )as b) as c natural join"
                + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
                + " asignatura_temp_as order by cod_estudiante ;";
         
         
         

        /*
        // Ciencias de la administracion
        
        sqlConsultaMatricula = "select cod_estudiante,id from"
                + " (select cod_est as cod_estudiante,codigo as cod_asignatura from "
                + " (select estudiante_key,codigo as cod_est from estudiante) as d natural join "
                + "(select estudiante_key, asignatura_key  from "
                + "(select *  from cancelacion natural join "
                + "(select programa_key from programa where codigo like '38%')as a )as b) as c natural join"
                + " asignatura group by cod_est,cod_asignatura order by cod_estudiante desc) as w natural join"
                + " asignatura_temp_as order by cod_estudiante ;";
        
        */



        String codigoEstudiante;


        int indexAsignatura;
        boolean primerCaso = true;

        try {
            Connection conn = fachada.conectar();
            Statement sentencia = conn.createStatement();
            ResultSet tabla = sentencia.executeQuery(sqlConsultaMatricula);

            while (tabla.next()) {
                codigoEstudiante = tabla.getString("cod_estudiante");
                indexAsignatura = tabla.getInt("id");
                if (primerCaso) {
                    estudiante = codigoEstudiante;
                    primerCaso = false;
                }

                System.out.println(codigoEstudiante + "- " + estudiante);
                if (estudiante.equals(codigoEstudiante)) {
                    asignaturas[indexAsignatura - 1] = 1;
                } else {
                    imprimirAsignaturas();
                    limpiarAsignaturas();
                    estudiante = codigoEstudiante;
                    asignaturas[indexAsignatura - 1] = 1;
                }
            }
            imprimirAsignaturas();
            fachada.cerrarConexion(conn);
        } catch (SQLException e) {
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    void limpiarAsignaturas() {
        for (int i = 0; i < cantidad; i++) {
            asignaturas[i] = 0;
        }
    }

    void imprimirAsignaturas() {
        try {
            String asignaturasString = asignaturasToString();
            file.write(asignaturasString);
            //System.out.println(asignaturasString);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    void cerrarArchivo() {
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    String asignaturasToString() {
        String asg = "";
        for (int i = 0; i < cantidad; i++) {
            asg += asignaturas[i];
            if (i != cantidad - 1) {
                asg += ",";
            }
        }

        asg += "\n";
        return asg;
    }

    public static void main(String arg[]) {
        createDataAsociation create = new createDataAsociation();
        create.poblarDatosTablaAsignatura();
        create.crearDatos(-1);
        create.cerrarArchivo();
    }
}